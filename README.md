# rpi4


## Retropie (default, tested image and kernel)


https://github.com/RetroPie/RetroPie-Setup/releases/download/4.8/retropie-buster-4.8-rpi4_400.img.gz


b5daa6e7660a99c246966f3f09b4014b  retropie-buster-4.8-rpi4_400.img.gz



95 bytes

b5daa6e7660a99c246966f3f09b4014b  retropie-buster-4.8-rpi4_400_1677623711-pi4-tested-ok.img.gz




````
pi@retropie:~ $ uname -a
Linux retropie 5.10.103-v7l+ #1529 SMP Tue Mar 8 12:24:00 GMT 2022 armv7l GNU/Linux
````

````
pi@retropie:~ $ cat /proc/cmdline 
coherent_pool=1M 8250.nr_uarts=0 snd_bcm2835.enable_compat_alsa=0 snd_bcm2835.enable_hdmi=1 video=HDMI-A-1:1920x1080M@60 smsc95xx.macaddr=E4:5F:01:76:71:5E vc_mem.mem_base=0x3ec00000 vc_mem.mem_size=0x40000000  console=ttyS0,115200 console=tty1 root=PARTUUID=b34c3f0e-02 rootfstype=ext4 fsck.repair=yes rootwait loglevel=3 consoleblank=0 plymouth.enable=0
````

````
pi@retropie:~ $ sudo blkid
/dev/mmcblk0p1: SEC_TYPE="msdos" LABEL_FATBOOT="boot" LABEL="boot" UUID="7B52-6C51" TYPE="vfat" PARTUUID="b34c3f0e-01"
/dev/mmcblk0p2: LABEL="retropie" UUID="e55989cf-283b-4cfc-8f49-8559e5ed331b" TYPE="ext4" PARTUUID="b34c3f0e-02"
/dev/mmcblk0: PTUUID="b34c3f0e" PTTYPE="dos"
````


````
Disk /dev/mmcblk0: 14.9 GiB, 15931539456 bytes, 31116288 sectors
Units: sectors of 1 * 512 = 512 bytes
Sector size (logical/physical): 512 bytes / 512 bytes
I/O size (minimum/optimal): 512 bytes / 512 bytes
Disklabel type: dos
Disk identifier: 0xb34c3f0e

Device         Boot  Start      End  Sectors  Size Id Type
/dev/mmcblk0p1 *      8192   532479   524288  256M  e W95 FAT16 (LBA)
/dev/mmcblk0p2      532480 31116287 30583808 14.6G 83 Linux
````
